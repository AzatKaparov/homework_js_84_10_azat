const express = require('express');
const Task = require("../models/Task");
const auth = require('../middleware/auth');
const myTask = require('../middleware/myTask');

const router = express.Router();

router.get("/", auth, async (req, res) => {
    const tasks = await Task.find({user: req.user.id}).populate("user", "id username");

    if (!tasks || tasks.length === 0) {
        return res.status(404).send({message: "Not found"});
    }
    try {
        res.send(tasks);
    } catch (e) {
        return res.status(500);
    }
});


router.post('/', auth, async (req, res) => {
    if (!req.body.title) {
        return res.status(400).send({message: "Invalid data!"});
    }

    const taskData = {
        title: req.body.title,
        description: req.body.description || null,
        status: req.body.status || undefined,
        user: req.user.id
    };

    try {
        const task = new Task(taskData);
        await task.save();
        return res.send(task);
    } catch (e) {
        res.status(500).send({message: e.message});
    }
});

router.put("/:id", [auth, myTask], async (req, res) => {
    try {
        const updateFields = req.body;
        if (updateFields.user) {
            delete updateFields["user"];
        }

        const updatedTask = await Task.findOneAndUpdate(
            {_id: req.params.id},
            updateFields,
            {
                new: true,
            });

        res.send(updatedTask);
    } catch (e) {
        return res.status(500);
    }
});

router.delete("/:id", [auth, myTask], (req, res) => {
    try {
        req.toUpdateTask.delete();
        res.send({message: "Deleted!"})
    } catch (e) {
        return res.status(500);
    }
});

module.exports = router;