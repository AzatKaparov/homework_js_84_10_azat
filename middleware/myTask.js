const Task = require('../models/Task');

const myTaskMiddleware = async (req, res, next) => {
    const task = await Task.findOne({_id: req.params.id});

    if (!task) {
        return res.status(400).send({message: "Not found!"});
    }

    if (!task.user.equals(req.user.id)) {
        return res.status(403).send({message: "This is not your task!"});
    }

    req.toUpdateTask = task;
    next();
};

module.exports = myTaskMiddleware;