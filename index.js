const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');
const users = require('./app/users');
const tasks = require('./app/tasks');

const app = express();
const port = 8000;

app.use(express.json());
app.use(cors());

app.use('/tasks', tasks);
app.use('/users', users);

const run = async () => {
    await mongoose.connect('mongodb://localhost/tasks', {useNewUrlParser: true});

    app.listen(port, () => {
        console.log(`Server started on ${port} port!`);
    });

    process.on('exit', async () => {
        console.log('exiting');
        await mongoDb.disconnect();
    });
};

run().catch(e => console.error(e));