const mongoose = require('mongoose');
const Shema = mongoose.Schema;

const TaskShema = new Shema({
    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: true,
    },
    title: {
        type: String,
        required: true,
    },
    description: {
        type: String
    },
    status: {
        type: String,
        enum: ['new', 'in_progress', 'complete'],
        default: 'new',
    },
});


const Task = mongoose.model("Task", TaskShema);
module.exports = Task;